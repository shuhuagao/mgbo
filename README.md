# MGBO

#### Description
Code for paper "Economic Cost and Carbon Emission Reduction of Microgrid via
Bi-Objective Optimization"

#### Structure
1. ./src/MGBO: the core package implementing algorithms
2. ./notebook: examples and visualization in the paper, which uses the MGBO package
3. ./data: input data in the case studies


#### Install

Tested with Julia 1.10.

1. Install Julia 1.10
2. Clone this Git repository.
3. Activate corresponding environments in Julia, then `instantiate` to install all dependencies of that environment, and finally run notebooks or use the MGBO package.
