# ϵ-constraint method

function build_payoff_table(input_data::ScenarioData; log_level::Int=0)
    payoff = zeros(2, 2)
    model = JuMP.Model(Cbc.Optimizer)
    set_optimizer_attribute(model, "logLevel", log_level)
    setup_opt_core(model, input_data)
    for (i, j) in [(1, 2), (2, 1)]
        # first minimize fi regardless of the other
        @objective(model, Min, model[Symbol("f$i")])
        optimize!(model)
        if termination_status(model) != JuMP.OPTIMAL
            @error "Global optimality failed in minimizing f$i: " termination_status(model)
        end
        # lexicographic optimization
        fi_star = objective_value(model)
        @constraint(model, fi_con, model[Symbol("f$i")] == fi_star)
        @objective(model, Min, model[Symbol("f$j")])
        optimize!(model)
        if termination_status(model) != JuMP.OPTIMAL
            @error "Global optimality failed in minimizing f$j lexicographically: " termination_status(model)
        end
        fji = objective_value(model)
        # remove the extra constraint to prepare the next optimization
        delete(model, fi_con)  # remove the extra constraint
        unregister(model, :fi_con)
        # fill the proper row of payoff table
        if i == 1
            payoff[1, :] = [fi_star, fji]
        else
            payoff[2, :] = [fji, fi_star]
        end
    end
    return payoff, model
end


function bi_objective_minimize(input_data::ScenarioData, ϵ::Float64, r::Float64; log_level::Int=0, ϕ_eps::Float64=1e-6)
    @assert r > 0
    model = JuMP.Model(Cbc.Optimizer)
    set_optimizer_attribute(model, "logLevel", log_level)
    setup_opt_core(model, input_data)
    # bring in the slack variable
    @variable(model, s >= 0)
    @constraint(model, model[:f2] + s == ϵ)
    @objective(model, Min, model[:f1] - ϕ_eps * s / r)
    optimize!(model)
    if termination_status(model) != JuMP.OPTIMAL
        @error "Global optimality failed: " termination_status(model)
    end
    return model
end