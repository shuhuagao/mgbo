# set up the core components in the bi-objective problem
# note that this does not generate a solvable problem in JuMP, but only the required ingredients

"""
    plf_approximate(f::Function, x_low, x_up, L::Int)

PLF approximation of a function `f` defined on [`x_low`, `x_up`] with `L` segments. 
Return the slope and intercept of each segment in two vectors.
"""
function plf_approximate(f::Function, x_low, x_up, L::Int)
    @assert x_low < x_up && L > 0
    x = range(x_low, x_up; length=L+1)
    k = zeros(L)
    b = zeros(L)
    for i = 1:L
        k[i] = (f(x[i+1]) - f(x[i])) / (x[i+1] - x[i])
        b[i] = f(x[i]) - k[i] * x[i]
    end
    return k, b
end

"""
    setup_opt_core(model::JuMP.Model, input_data::ScenarioData, K::Int=10)

Set up the core constraints in bi-objective optimization. The two objectives are stored as `f1` and `f2` in the model.
"""
function setup_opt_core(model::JuMP.Model, input_data::ScenarioData, K::Int=10)
    @unpack P_PV, P_WT, P_L, P_CL, ρ_UG_b, ρ_UG_s = input_data
    @assert length(P_PV) == length(P_WT) == length(P_L) == length(ρ_UG_b) == length(ρ_UG_s) == length(P_CL) == T
    # MT
    @variable(model, 0 <= P_MT[1:T] <= P_MT_max)

    # BS
    @variable(model, P_BS_c[1:T] >= 0)
    @variable(model, P_BS_d[1:T] >= 0)
    @variable(model, ζ_BS_c[1:T], Bin)
    @variable(model, ζ_BS_d[1:T], Bin)
    @constraint(model, P_BS_c .<= ζ_BS_c .* P_BS_c_max)
    @constraint(model, P_BS_d .<= ζ_BS_d .* P_BS_d_max)
    @constraint(model, ζ_BS_c .+ ζ_BS_d .<= 1)
    # SOC dynamics
    @expression(model, SOC_BS, zeros(AffExpr, T))
    # https://jump.dev/JuMP.jl/stable/tutorials/getting_started/performance_tips/#Use-macros-to-build-expressions
    SOC_BS[1] = @expression(model, SOC_BS_ref + P_BS_c[1] * Δt * η_BS_c / E_BS - P_BS_d[1] * Δt / (η_BS_d * E_BS))
    for t = 2:T
        SOC_BS[t] = @expression(model, SOC_BS[t-1] + P_BS_c[t] * Δt * η_BS_c / E_BS - P_BS_d[t] * Δt / (η_BS_d * E_BS))
    end
    @constraint(model, SOC_BS_min .<= SOC_BS .<= SOC_BS_max)
    @constraint(model, SOC_BS[T] == SOC_BS_ref)

    # power exchange with utility grid
    @variable(model, P_UG_b[1:T] >= 0)
    @variable(model, P_UG_s[1:T] >= 0)
    @variable(model, ζ_UG_b[1:T], Bin)
    @variable(model, ζ_UG_s[1:T], Bin)
    @constraint(model, P_UG_b .<= P_UG_b_max .* ζ_UG_b)
    @constraint(model, P_UG_s .<= P_UG_s_max .* ζ_UG_s)
    @constraint(model, ζ_UG_b .+ ζ_UG_s .<= 1)

    # load shedding 
    @variable(model, P_shed[1:T] >= 0)
    @constraint(model, P_shed .<= P_L .- P_CL)
    
    # power balance
    @constraint(model, P_PV .+ P_WT .+ P_MT .+ P_BS_d .+ P_UG_b .== 
                       P_BS_c .+ P_UG_s .+ P_L .- P_shed)

    # economic cost
    ## MT
    k, b = plf_approximate(P->a_MT*P^2 + b_MT*P + c_MT, 0, P_MT_max, K) # K×1
    @variable(model, C_MT[1:T])
    @constraint(model, C_MT .>= (P_MT .* k' .+ b') .* Δt)  # Tx1 >= TxK, broadcasting
    @expression(model, C_MT_exact, (a_MT .* P_MT .^ 2 .+ b_MT .* P_MT .+ c_MT) .* Δt)  # exact quadratic cost
    ## UG
    @expression(model, C_UG, ρ_UG_b .* P_UG_b .* Δt .- ρ_UG_s .* P_UG_s .* Δt)
    ## load shedding
    @expression(model, C_shed, λ_shed .* P_shed)
    @expression(model, C, C_MT .+ C_UG .+ C_shed)

    # carbon emission
    @expression(model, E, (γ_MT .* P_MT .+ γ_UG .* P_UG_b - (γ_UG - γ̄_MG) .* P_UG_s) .* Δt)

    # two objectives
    model[:f1] = @expression(model, sum(C))
    model[:f2] = @expression(model, sum(E))

    # other dependent variables to facilitate subsequent analysis 
    model[:P_UG] = @expression(model, model[:P_UG_b] .- model[:P_UG_s])  # positive: buy, neg: sell
    model[:P_BS] = @expression(model, model[:P_BS_d] .- model[:P_BS_c])  # positive: discharge, neg: charge
end