# Parameters of the microgrid related to bi-objective optimization

const P_MT_max = 50
const a_MT = 0.01
const b_MT = 0.05
const c_MT = 0.0

const η_BS_c = 0.95
const η_BS_d = 0.94
const E_BS = 100.0
const SOC_BS_min = 0.1
const SOC_BS_max = 0.9
const SOC_BS_ref = 0.5
const P_BS_c_max = 20.0
const P_BS_d_max = 20.0

const P_UG_b_max = 100.0
const P_UG_s_max = 100.0
const β_UG = 0.85

const γ_MT = 1.0
const γ̄_MG = 0.6
const γ_UG = 0.8

const λ_shed = 1.0
const P_CL_ratio = 0.7

const T = 24
const Δt = 24 / T