# bi-objective TOPSIS

"""
    run_TOPSIS(E::AMF, w::AVF)

`E` is the raw decision matrix of size `n \times c`, and `w` is the weight vector of length `c`.
Here, `n` is the number of alternatives, and `c` refers to the number of criteria.
A `Dict` called `res` containing intermediate results is returned, e.g., `res[:sb]` gives the similarity.
"""
function run_TOPSIS(E::AMF, w::AVF)::AbstractDict
    res = Dict{Symbol, AVMF}()
    n, c = size(E)
    @assert c == length(w)   # number of criteria
    μ = mean(E, dims=1)
    σ = std(E, dims=1)
    V = (E .- μ) ./ σ
    res[:V] = V
    R = w' .* V  # nx2
    res[:R] = R
    ext = extrema(R; dims=1)
    Aw = [ext[1][2], ext[2][2]]  
    Ab = [ext[1][1], ext[2][1]]
    res[:Aw] = Aw
    res[:Ab] = Ab
    dw = colwise(Euclidean(), R', Aw)  # n vector
    db = colwise(Euclidean(), R', Ab)   # n vector
    res[:dw] = dw
    res[:db] = db
    sb = 1 .- db ./ (db .+ dw)
    res[:sb] = sb
    return res
end