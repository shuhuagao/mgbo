# single-objective optimization


function single_objective_minimize(input_data::ScenarioData, which::Int; log_level::Int=0)
    model = JuMP.Model(Cbc.Optimizer)
    set_optimizer_attribute(model, "logLevel", log_level)
    setup_opt_core(model, input_data)
    # first objective 
    @objective(model, Min, model[Symbol("f", which)])
    optimize!(model)
    if termination_status(model) != JuMP.OPTIMAL
        @error "Global optimality failed in minimizing f$which: " termination_status(model)
    end
    key_res = :result
    model[key_res] = Dict{Symbol, Union{AbstractFloat, AVF}}()
    model[key_res][:P_MT] = value.(model[:P_MT])
    model[key_res][:P_UG] = value.(model[:P_UG_b] .- model[:P_UG_s])  # positive: buy, neg: sell
    model[key_res][:P_BS] = value.(model[:P_BS_d] .- model[:P_BS_c])  # positive: discharge, neg: charge
    model[key_res][:P_shed] = value.(model[:P_shed])
    model[key_res][Symbol("f", which)] = objective_value(model)
    other = which == 1 ? 2 : 1
    model[key_res][Symbol("f", other)] = value(model[Symbol("f", other)])
    return model
end


function weighted_single_objective_minimize(input_data::ScenarioData, weights::AVF; log_level::Int=0)
    model = JuMP.Model(Cbc.Optimizer)
    set_optimizer_attribute(model, "logLevel", log_level)
    setup_opt_core(model, input_data)
    # weighted objective
    @objective(model, Min, sum(model[Symbol("f", i)] * weights[i] for i in 1:2))
    optimize!(model)
    if termination_status(model) != JuMP.OPTIMAL
        @error "Global optimality failed: " termination_status(model)
    end
    key_res = :result
    model[key_res] = Dict{Symbol, Union{AbstractFloat, AVF}}()
    model[key_res][:P_MT] = value.(model[:P_MT])
    model[key_res][:P_UG] = value.(model[:P_UG_b] .- model[:P_UG_s])  # positive: buy, neg: sell
    model[key_res][:P_BS] = value.(model[:P_BS_d] .- model[:P_BS_c])  # positive: discharge, neg: charge
    model[key_res][:P_shed] = value.(model[:P_shed])
    model[key_res][:weighted_f] = objective_value(model)
    model[key_res][:f1] = value(model[:f1])
    model[key_res][:f2] = value(model[:f2])
    return model
end