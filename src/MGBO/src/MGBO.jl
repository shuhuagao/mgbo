module MGBO

using JuMP, Cbc
using CSV
using UnPack
using Statistics, Distances

const AVF = AbstractVector{<:AbstractFloat}
const AMF = AbstractMatrix{<:AbstractFloat}
const AVMF = AbstractVecOrMat{<:AbstractFloat}
const get_value = JuMP.value

Base.@kwdef struct ScenarioData
    P_PV::Vector{Float64}
    P_WT::Vector{Float64}
    P_L::Vector{Float64}
    P_CL::Vector{Float64}
    ρ_UG_b::Vector{Float64}
    ρ_UG_s::Vector{Float64}
end

include("parameters.jl")
include("opt_core.jl")
include("soopt.jl")
include("ecm.jl")
include("topsis.jl")

export ScenarioData, get_value, single_objective_minimize, bi_objective_minimize, run_TOPSIS,
weighted_single_objective_minimize

end # module MGBO
